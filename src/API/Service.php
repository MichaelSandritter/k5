<?php
/**
 * Created by PhpStorm.
 * User: michaelsandritter
 * Date: 13/11/15
 * Time: 11:59
 */
namespace Komponente5\API;

class Service {

    private $content;

    function __construct()
    {
        $this->content = 'component 5';
    }

    public function getContent()
    {
        return $this->content;
    }
}